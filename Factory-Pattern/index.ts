function Developer(name: string) {
  this.name = name;
  this.type = "Developer";
}

function Tester(name:string) {
  this.name = name;
  this.type = "Tester";
}

function EmployeeFactory() {
  this.create = (name:string, type) => {
    switch (type) {
      case 1:
        return new Developer(name);
        break;
      case 2:
        return new Tester(name);
        break;
    }
  }
}
function display(){
          console.log(`Hi Im ${this.name} and Im a ${this.type}.`)
}

const employeeFactory = new EmployeeFactory();
const employees = [];
employees.push(employeeFactory.create("Sagar", 1));
employees.push(employeeFactory.create("John",2));
employees.push(employeeFactory.create("Vivek",1));
for(let employee of employees){
          display.call(employee);
}