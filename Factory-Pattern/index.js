function Developer(name) {
    this.name = name;
    this.type = "Developer";
}
function Tester(name) {
    this.name = name;
    this.type = "Tester";
}
function EmployeeFactory() {
    this.create = function (name, type) {
        switch (type) {
            case 1:
                return new Developer(name);
                break;
            case 2:
                return new Tester(name);
                break;
        }
    };
}
function display() {
    console.log("Hi Im ".concat(this.name, " and Im a ").concat(this.type, "."));
}
var employeeFactory = new EmployeeFactory();
var employees = [];
employees.push(employeeFactory.create("Sagar", 1));
employees.push(employeeFactory.create("John", 2));
employees.push(employeeFactory.create("Vivek", 1));
for (var _i = 0, employees_1 = employees; _i < employees_1.length; _i++) {
    var employee = employees_1[_i];
    display.call(employee);
}
