function Member(name) {
          this.name = name;
          this.chatroom = null;
      }
      
      Member.prototype = {
          send: function(message, toMember) {
              this.chatroom.send(message, this, toMember)
          },
          recieve: function(message, fromMember) {
              console.log(`${fromMember.name} to ${this.name}:${message}`);
          }
      }
      
      function chatroom() {
          this.members = {};
      }
      
      chatroom.prototype = {
          addMember: function(member) {
              this.members[member.name] = member
              member.chatroom = this
          },
          send: function(message, fromMember, toMember) {
              toMember.recieve(message, fromMember)
          }
      }
      
      const chat = new chatroom()
      
      const sagar = new Member("Sagar")
      const viraj = new Member("Viraj")
      const tejas = new Member("Tejas")
      
      chat.addMember(sagar)
      chat.addMember(viraj)
      chat.addMember(tejas)
      
      sagar.send("Hey viraj", viraj);