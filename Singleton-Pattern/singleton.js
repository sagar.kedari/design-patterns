const Singleton = (function(){
    let pManager
  
    function ProcessManager() { /*...*/ }
  
    function createProcessManager()
    {
      pManager = new ProcessManager()
      return pManager
    }
  
    return {
        getProcessManager: () =>
        {
          if(!pManager)
            pManager = createProcessManager()
          return pManager
        }
    }
  })()
 
  const singleton1 = Singleton.getProcessManager()
  const singleton2 = Singleton.getProcessManager()
  console.log(singleton1)
  console.log(singleton2)
  console.log(singleton1 === singleton2)