function Subject() {
  this.obserevers = [];
}

Subject.prototype = {
  subscribe: function (fn) {
    this.obserevers.push(fn);
  },

  unsubcribe: function (funcToRemove) {
    this.obserevers = this.obserevers.filter((fn) => {
      if (fn != funcToRemove) {
        return fn;
      }
    });
  },

  triger: function () {
    this.obserevers.forEach((fn) => fn.call());
  },
};

const subject = new Subject()

function Observer1(){
          console.log("Observer: trigering")
}

subject.subscribe(Observer1)
subject.triger();